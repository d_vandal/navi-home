const Header = ({createToggle, logo}) => {
  return (
    <header className="header">
      <a href="/">
      <div className="text-2xl p-2">
        <img className="inline" src={logo} width={'40px'}/>
        <span className="text-blue-500">N</span>avi<span className="text-red-500">H</span>ome
      </div>
      </a>
      <div></div>
      <div className="header-button-outer">
        <div className="header-button p-3" onClick={createToggle}>New Upload</div>
      </div>
    </header>
  )
}

export default Header
