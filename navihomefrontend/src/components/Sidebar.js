import Search from './Search'

const Sidebar = ({submitQuery}) => {
  return (
    <div className="sidebar">
      <Search submitQuery={submitQuery} />
      <div className='sidebar-content'>
        <h1>Sidebar Content</h1>
      </div>
    </div>
  )
}

export default Sidebar
