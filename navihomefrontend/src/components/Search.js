import { useState } from 'react'

const Search = ({submitQuery}) => {
  const [query, setQuery] = useState('')
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('A query was submitted: ' + query);
    submitQuery(query)
  }

  return (
    <div className="searchBar">
      <form onSubmit={handleSubmit}>
        <input type="text<br/>" placeholder="comma, separated, tags..." value={query} onChange={(e) => {setQuery(e.target.value)}}/>
        <input type="submit" value="search" />
      </form>
    </div>
  )
}

export default Search
