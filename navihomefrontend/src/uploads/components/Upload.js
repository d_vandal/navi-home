import {useState } from 'react'
import EditUpload from './EditUpload'
import '../Upload.css';

// Target Backend Server
const target = `${process.env.REACT_APP_TARGET}`

const Upload = ({upload, editUpload, deleteUpload, showUpload}) => {
  const [editToggle, setEditToggle] = useState(false)
  const [deleteConfirm, setDeleteConfirm] = useState(false)

  const handleDelete = (e) => {
    console.log(e)
    deleteUpload(e)
  }

  return (
    <div id={upload.id} className="upload-card h-full">
      <div className="upload-card-title">      
        {upload.title}
      </div>
      <div className="">
        <div className="upload-content-container">
        {editToggle ? (
          <EditUpload editUpload={editUpload} upload={upload} toggleEdit={setEditToggle} />
        ):(
          <>
            <div className="picture-container" style={{'height':'200px'}}>
              <img src={`${target}${upload.upload_object}`} />
            </div>
            <div className="upload-card-info">
              <p>Description:</p>
              <div className="bg-white border border-gray-500 m-1">
                <p className="text-sm">{upload.description}</p>
              </div>

              <div className="tags-container">
                Tags: {upload.all_tags}
              </div>
            </div>
          </>
        )}
        </div>
      </div>
      <div className='bg-white bottom-0 w-full mt-auto'>
        <button onClick={(e) => {showUpload(upload)}} >show</button>
        <button onClick={(e) => {setEditToggle(!editToggle)}}>{editToggle ? 'cancel':'edit'}</button>
        {deleteConfirm ? (
            <>
            Are you sure?
            <button onClick={(e) => {handleDelete(upload.id)}}>yes</button>
            <button onClick={(e) => {(setDeleteConfirm(!deleteConfirm))}}>no</button>
            </>
          ) : (
            <button onClick={(e) => {(setDeleteConfirm(!deleteConfirm))}}>delete</button>
          )}
      </div>
    </div>
  )
}

export default Upload
