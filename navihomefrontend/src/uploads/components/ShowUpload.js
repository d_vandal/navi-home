import '../Upload.css';

// Target Backend Server
const target = `${process.env.REACT_APP_TARGET}`

const ShowUpload = ({upload, toggleShow}) => {
  const uploadFile = () => {
    console.log(upload.type)
    if (upload.type === 'image'){
      return <img src={`${target}${upload.upload_object}`} />
    } else if (upload.type === 'video'){
      return <video src={`${target}${upload.upload_object}`} controls />
    } else {
      <div>Content goes here loser</div>
    }
  }

  return (
    <div className="ml-auto h-full mr-auto w-full md:w-1/2 border border-gray-500 bg-gray-200">
      <div id={upload.id}>
      <div className="upload-card-title">      
        {upload.title}
      </div>
      <div>
        <div className="">
              {uploadFile()}
              <p>Description:</p>
              <div className="bg-white border border-gray-500 m-1">
                <p className="text-sm">{upload.description}</p>
              </div>

              <div className="tags-container">
                Tags: {upload.all_tags}
              </div>
            </div>
          <button onClick={(e) => {toggleShow()}}>close</button>
        </div>
      </div>
    </div>
  )
}

export default ShowUpload
