import { useState } from 'react'

const CreateUpload = ({newUpload, upload}) => {
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [upload_object, setUploadObject] = useState('')
  const [all_tags, setAllTags] = useState('')
  
  const onSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData()
    
    formData.append('title', title)
    formData.append('description', description)
    formData.append('upload_object', upload_object)
    formData.append('all_tags', all_tags)
    

    newUpload(formData)
    setTitle('')
    setDescription('')
    setUploadObject('')
    setAllTags('')
  };

  return (
      <div className="formContainer">
        <div className="formContainerTitle">Create Upload</div>
        <form onSubmit={onSubmit}>
          <div>
            <label>
              Title:
              <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
            </label>
          </div>
          <div>
            <label>
              Description:
              <input type="text" value={description} onChange={(e) => setDescription(e.target.value)} />
            </label>
          </div>
          <div>
            <label>
              All Tags:
              <input type="text" placeholder="Enter comma separated tags..." value={all_tags} onChange={(e) => setAllTags(e.target.value)} />
            </label>
          </div>

        <input type="file" onChange={(e) => setUploadObject(e.target.files[0])} />
        <button>submit</button>
      </form>
    </div>
  )
}

export default CreateUpload