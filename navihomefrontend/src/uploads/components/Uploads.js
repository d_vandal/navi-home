import '../Upload.css';
import Upload from './Upload'

const Uploads = ({uploads, editUpload, deleteUpload, showUpload}) => {
  return (
  <div className="uploads-container">
    {uploads.map((upload) => (
        <Upload key={upload.id} upload={upload} editUpload={editUpload} deleteUpload={deleteUpload} showUpload={showUpload} />
        ))}
  </div>
  )
}

export default Uploads