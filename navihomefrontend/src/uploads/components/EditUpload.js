import { useState } from 'react'

// Target Backend Server
const target = `${process.env.REACT_APP_TARGET}`

const EditUpload = ({editUpload, upload, toggleEdit}) => {
  const [title, setTitle] = useState(upload.title)
  const [description, setDescription] = useState(upload.description)
  const [upload_object, setUploadObject] = useState(upload.upload_object)
  const [all_tags, setAllTags] = useState(upload.all_tags)
  
  const onSubmit = (e) => {
    e.preventDefault();
    // Handle if the file doesn't change
    const formData = new FormData()
    formData.append('title', title)
    formData.append('description', description)
    if (upload.upload_object != upload_object) {
      formData.append('upload_object', upload_object)
    }
    formData.append('all_tags', all_tags)
  
    editUpload(formData, upload.id)

    toggleEdit(false)
  };

  return (
      <div className="editFormContainer text-left overflow-y-auto overflow-x-hidden">
        <div>Editing Upload: {title}</div>
        <form onSubmit={onSubmit}>
          <div>
            <label>
              Title:<br/>
              <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
            </label>
          </div>
          <div>
            <label>
              Description:<br/>
              <input type="text" value={description} onChange={(e) => setDescription(e.target.value)} />
            </label>
          </div>
          <div>
            <label>
              All Tags:<br/>
              <input type="text" placeholder="Enter comma separated tags..." value={all_tags} onChange={(e) => setAllTags(e.target.value)} />
            </label>
          </div>
          <img className="border border-gray-500" src={`${target}${upload.upload_object}`} width='50' height='50' />
        <input type="file" onChange={(e) => setUploadObject(e.target.files[0])} />
        <button>submit changes</button>
      </form>
    </div>
  )
}

export default EditUpload