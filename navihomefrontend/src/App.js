import {useState, useEffect } from 'react'
import logo from './logo.png';
import './App.css';
// Components
import Header from './components/Header'
import Sidebar from './components/Sidebar'
import Search from './components/Search'
import Uploads from './uploads/components/Uploads'
import CreateUpload from './uploads/components/CreateUpload';
import ShowUpload from './uploads/components/ShowUpload'

// Target Backend Server
const target = `${process.env.REACT_APP_TARGET}`

function App() {
  const [uploads, setUploads] = useState([])
  const [toggleCreate, setCreateToggle] = useState(false)
  const [toggleShow, setToggleShow] = useState(false)
  const [currentlyViewing, setCurrentlyViewing] = useState('')

  useEffect(() => {
    const getUploads = async () => {
      const uploadsFromServer = await fetchUploads()
      setUploads(uploadsFromServer)
    }
    getUploads()
  }, [])

    // TODO: Handle other response codes
    // GET /uploads
  const fetchUploads = async () => {
    const res = await fetch(`${target}/uploads`, {
      mode: 'cors'
    })
    const data = await res.json()

    return data
  }

    // TODO: Handle other response codes
  const submitQuery = async (query) => {
    console.log(query)
    const res = await fetch(`${target}/search?query=${query}`)
    const data = await res.json()

    console.log(data)
    setUploads(data)
  }

  // TODO: Handle other response codes
  // POST uploads/new
  const newUpload = async (upload) => {
    console.log('Submitting new upload')

    const res = await fetch(`${target}/uploads/`, {
      method: 'POST',
      body: upload
    })

    if (res.status === 200){
      const data = await res.json()
      console.log([...uploads, data])
      setUploads([...uploads, data])
      setCreateToggle(!toggleCreate)
      setToggleShow(!toggleShow)
      setCurrentlyViewing(data)
    }
  }

  // TODO: Handle other response codes
  // POST uploads/{id}
  const updateUpload = async (upload, id) => {
    console.log('Submitting edited upload')
    console.log(upload)
    const res = await fetch(`${target}/uploads/${id}`, {
      method: 'PUT',
      body: upload
    })

    if (res.status === 200){
      const data = await res.json()
      
      // Loops through the uploads held in state and creates a new array
      // replacing the changed element with the response from the server
      var updatedUploads = []
      for (let i=0;i<uploads.length;i++){
        if (uploads[i].id !== id){
          updatedUploads.push(uploads[i])
        } else {
          updatedUploads.push(data)
        }
      }

      setUploads(updatedUploads)
    }
  }

  // TODO: Handle other response codes
  // DELETE uploads/{id}
  const deleteUpload = async (id) => {
    console.log(`DELETE: ${id}`)
    const res = await fetch(`${target}/uploads/${id}`, {
      method: 'DELETE'
    })
    console.log(res.status)
    console.log(id)

    if (res.status === 204){
      // Loops through the uploads held in state and creates a new array
      // replacing the changed element with the response from the server
      // This avoids mutating the state
      var updatedUploads = []
      for (let i=0;i<uploads.length;i++){
        if (uploads[i].id !== id){
          updatedUploads.push(uploads[i])
        }
      }

      console.log(updatedUploads)
      setUploads(updatedUploads)

    }
  }

  // setToggleShow(!toggleShow)
  // setCurrentlyViewing(data)
  const showUpload = async (upload) => {
    const res = await fetch(`${target}/uploads/${upload.id}`)
    const data = await res.json()

    setCurrentlyViewing(data)
    setToggleShow(!toggleShow)
  }

  return (
    <div className="App">
      <Header createToggle={() => {setCreateToggle(!toggleCreate)}} logo={logo} />
      <div className='content-container'>
        <Sidebar submitQuery={submitQuery} />
        <div className='overflow-y-auto w-full' style={{'height':'95vh'}}>
          {toggleShow ? (<ShowUpload upload={currentlyViewing} toggleShow={setToggleShow} />)
          :(
            <>
            { toggleCreate && <CreateUpload newUpload={newUpload} /> }
            <Uploads uploads={uploads} editUpload={updateUpload} deleteUpload={deleteUpload} showUpload={showUpload} />
            </>
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
