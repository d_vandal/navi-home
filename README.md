# Navi Home

This is a web application for providing a home file server, with plans to introduce additional features as I decide to add them.

The application is split into two sections, the frontend and the backend.

The frontend is a ReactJS application, which pulls and presents data from the backend.

The backend is a Ruby on Rails API application, which serves to handle incoming uploads and so forth.

This is a variation of a previous project with the same goals, but with the addition of separating the frontend from the backend to help with writing the application.