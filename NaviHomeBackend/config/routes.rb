Rails.application.routes.draw do
  resources :collections do
    resources :uploads
  end

  resources :uploads
  #root 'home#index'

  get 'search', to: 'search#search', s: "search"
end
