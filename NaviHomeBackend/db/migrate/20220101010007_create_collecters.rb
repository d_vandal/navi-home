class CreateCollecters < ActiveRecord::Migration[7.0]
  def change
    create_table :collecters do |t|
      t.references :collection, null: false, foreign_key: true
      t.references :upload, null: false, foreign_key: true

      t.timestamps
    end
  end
end
