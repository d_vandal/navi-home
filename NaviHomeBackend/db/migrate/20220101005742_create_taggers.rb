class CreateTaggers < ActiveRecord::Migration[7.0]
  def change
    create_table :taggers do |t|
      t.references :upload, null: false, foreign_key: true
      t.references :tag, null: false, foreign_key: true

      t.timestamps
    end
  end
end
