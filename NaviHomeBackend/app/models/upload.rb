class Upload < ApplicationRecord
  mount_uploader :upload_object, FileUploader
  #serialize :upload_file, JSON
  validates_presence_of :title, :upload_object
  has_many :taggers, dependent: :delete_all
  has_many :tags, through: :taggers
  has_many :collections, through: :collecters

    # This handles both creating the new tags and
  # mapping them to the Part through Tagging
  def all_tags=(name)
    self.tags = name.split(",").map do |name|
      Tag.where(name: name.strip.downcase).first_or_create!
    end
  end

  # Gets all of the tags under a part and converts all
  # of the names to a comma delimited string.
  # This is mainly for use in views
  def all_tags
    self.tags.map(&:name).join(", ")
  end

  #
  def self.tagged_with(name)
    if Tag.find_by_name(name) != nil
      Tag.find_by_name(name).uploads
    else
      return []
    end
  end

  private
    def update_upload_file_attributes
      if upload_object.present? && upload_object_changed?
        self.content_type = upload_object.file.content_type
        self.file_size = upload_object.file.size
      end
    end
end
