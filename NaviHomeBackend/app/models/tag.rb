class Tag < ApplicationRecord
  has_many :taggers
  has_many :uploads, through: :taggers
  has_many :collections, through: :taggers
end
