class Collecter < ApplicationRecord
  has_many :collecters, dependent: :delete_all
  has_many :uploads, through: :collecters

  has_many :taggers, dependent: :delete_all
  has_many :tags, through: :taggers
  has_rich_text :description

  def all_uploads=(ids)
    self.uploads = []
    ids.split(",").uniq.each do |id|
      self.uploads.append(Upload.find(id)) if Upload.exists?(id)
    end
  end

  def all_uploads
    self.uploads.map(&:id).join(", ")
  end

  # This handles both creating the new tags and
  # mapping them to the Part through Tagging
  def all_tags=(name)
    self.tags = name.split(",").map do |name|
      Tag.where(name: name.strip.downcase).first_or_create!
    end
  end

  # Gets all of the tags under a part and converts all
  # of the names to a comma delimited string.
  # This is mainly for use in views
  def all_tags
    self.tags.map(&:name).join(", ")
  end

  #
  def self.tagged_with(name)Tag.first
    if Tag.find_by_name(name) != nil
      Tag.find_by_name(name).collections
    else
      return []
    end
  end
end
