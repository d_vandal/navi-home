class Tagger < ApplicationRecord
  belongs_to :upload, optional: true
  belongs_to :collection, optional: true
  belongs_to :tag
end
