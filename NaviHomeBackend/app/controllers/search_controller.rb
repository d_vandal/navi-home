class SearchController < ApplicationController
  # GET /search/query=<query>&commit=Search
  # Utilizes the tagged_with method off of Uploads to get every Upload with the supplied tag
  # it then trims the list by making sure all searched tags are present on the item
  # Then prunes the list of any duplicates
  # It returns the search.js.erb
  def search
    #binding.break
    @query = params[:query].downcase
  
    @collections = []
    #binding.break

    if @query != ""
      tags = @query.split(",").map
      tags.each do |tag|
        @uploads.nil? ? @uploads = Upload.tagged_with(tag.strip) : @uploads += Upload.tagged_with(tag.strip)
        #@uploads += Upload.tagged_with(tag.strip)
        #@collections += Collection.tagged_with(tag.strip)
      end
      # tags.each do |tag|
      #   @uploads.delete_if { |upload| !upload.all_tags.include? tag.strip }
      #   @collections.delete_if { |collection| !collection.all_tags.include? tag.strip }
      # end
    else
      @uploads = Upload.all
      #@collections = Collection.all
    end



    @uploads = @uploads.uniq
    #@collections = @collections.uniq
  end
end


