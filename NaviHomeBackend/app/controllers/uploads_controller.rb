# putting here so I can stop googling this
# binding.break
class UploadsController < ApplicationController
  before_action :set_upload, only: %i[ show update destroy ]

  # GET /uploads
  def index
    @uploads = Upload.all

    # Rendering the json using JBuilder
    # leaving as reference
    #render json: @uploads
  end

  # GET /uploads/1
  def show
    render @show
  end

  # POST /uploads
  def create
    @upload = Upload.new(upload_params)
    #binding.break

    if @upload.save
      render @create
    else
      render json: @upload.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /uploads/1
  def update
    upload = @upload  # TODO: ??? :/
                      # I know why, just can't think of a pretty solution atm
    if upload.update(upload_params)
      render upload
    else
      render json: upload.errors, status: :unprocessable_entity
    end
  end

  # DELETE /uploads/1
  def destroy
    #binding.break
    @upload.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_upload
      @upload = Upload.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def upload_params
      params.permit(:title, :description, :upload_object, :all_tags)
    end

    
end
