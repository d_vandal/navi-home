json.id @upload.id
json.title @upload.title
json.description @upload.description

  # handle linking the image
  if @upload.upload_object.content_type.include? 'image'
    json.type 'image'
  else
    json.type 'video'
  end
  json.upload_object url_for(@upload.upload_object.url)
  # elsif @upload.upload_object.content_type.include? 'video'
  #   json.upload_object url_for(@upload.upload_object.url)
  #   json.type 'video'
  # else
  #   json.upload_object url_for('/not-representable.png') # just serve the file from public/
  #   json.type 'unknown'
  # end

json.all_tags @upload.all_tags
