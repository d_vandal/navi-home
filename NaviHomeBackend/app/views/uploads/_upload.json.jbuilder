json.id upload.id
json.title upload.title
json.description upload.description

#binding.break

  # handle linking the image
  if upload.upload_object.content_type.include? 'image' #upload.upload_object.versions.empty?
    json.upload_object upload.upload_object.url(:thumb)
  else 
    json.upload_object url_for('/not-representable.png')
    
  # else
  #   json.upload_object upload.upload_object.url(:preview)
  end

json.all_tags upload.all_tags
